package com.example.myweapondbapp;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    SQLiteDatabase db;

    Button buttonAddWeapon, buttonClearAllWeapons;
    EditText editTextName, editTextMaterial, editTextPrice, editTextNote;

    ListView listViewWeapons;

    private void UpdateListViewWeapons()
    {
        ArrayList<String> weapons = new ArrayList<>();

        Cursor cursor = db.rawQuery("SELECT * FROM weapons", null);

        if(cursor.moveToFirst()==true)
        {
            do
            {
                String name = cursor.getString(1);
                String material = cursor.getString(2);
                String price = cursor.getString(3);
                String note = cursor.getString(4);

                weapons.add(name+"\n"+material+"\n"+price+"\n"+note);
            }while (cursor.moveToNext()==true);
        }

        cursor.close();

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, weapons);

        listViewWeapons.setAdapter(adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextName = findViewById(R.id.editTextName);
        editTextMaterial = findViewById(R.id.editTextMaterial);
        editTextPrice = findViewById(R.id.editTextPrice);
        editTextNote = findViewById(R.id.editTextNote);

        buttonAddWeapon = findViewById(R.id.buttonAddWeapon);
        buttonAddWeapon.setOnClickListener(buttonAddWeaponClickListener);

        buttonClearAllWeapons = findViewById(R.id.buttonClearAllWeapons);
        buttonClearAllWeapons.setOnClickListener(buttonClearAllWeaponsClickListener);

        listViewWeapons = findViewById(R.id.listViewWeapons);

        db = getBaseContext().openOrCreateDatabase("app.db", MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS \"weapons\" (\n" +
                "\t\"id\"\tINTEGER NOT NULL,\n" +
                "\t\"name\"\tTEXT NOT NULL,\n" +
                "\t\"material\"\tTEXT NOT NULL,\n" +
                "\t\"price\"\tTEXT NOT NULL,\n" +
                "\t\"note\"\tTEXT NOT NULL,\n" +
                "\tPRIMARY KEY(\"id\" AUTOINCREMENT)\n" +
                ")");

        UpdateListViewWeapons();
    }

    View.OnClickListener buttonAddWeaponClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String name = editTextName.getText().toString();
            String material = editTextMaterial.getText().toString();
            String price = editTextPrice.getText().toString();
            String note = editTextNote.getText().toString();

            if(name.equals(""))
            {
                Toast.makeText(getApplicationContext(), "Input Name!", Toast.LENGTH_LONG).show();
                return;
            }
            if(material.equals(""))
            {
                Toast.makeText(getApplicationContext(), "Input Material!", Toast.LENGTH_LONG).show();
                return;
            }
            if(price.equals(""))
            {
                Toast.makeText(getApplicationContext(), "Input Price!", Toast.LENGTH_LONG).show();
                return;
            }

            db.execSQL("INSERT INTO weapons (name, material, price, note)\n" +
                    "VALUES ('"+name+"', '"+material+"', '"+price+"', '"+note+"')");

            Toast.makeText(getApplicationContext(), "Weapon inserted", Toast.LENGTH_LONG).show();

            UpdateListViewWeapons();

            editTextName.getText().clear();
            editTextMaterial.getText().clear();
            editTextPrice.getText().clear();
            editTextNote.getText().clear();
        }
    };

    View.OnClickListener buttonClearAllWeaponsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            db.execSQL("DELETE FROM weapons");

            Toast.makeText(getApplicationContext(), "Weapons deleted", Toast.LENGTH_LONG).show();

            UpdateListViewWeapons();
        }
    };
}
